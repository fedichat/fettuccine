use crate::config::SERVER_NAME;
use std::collections::HashMap;
use serde::{Serialize, Deserialize};
use serde_json::Value;

#[derive(Serialize,Deserialize,Debug,Clone)]
pub struct Message {
    // This is only guaranteed for server->client messages not client->server
    // message_id: u64,
    #[serde(rename = "type")] 
    ty: String,
    body: String,
    #[serde(flatten)]
    custom_fields: HashMap<String,Value>
}

impl Message {
    pub fn add_id(mut self, id: u64) -> Message {
        self.custom_fields.insert("message_id".to_string(),Value::Number(id.into()));
        self
    }
    pub fn stamp(&mut self) {
        // Turn the server name into a JSON string
        let name = Value::String(SERVER_NAME.to_owned());

        // Check to see if the message's origin has already been stamped
        match self.custom_fields.get("origin") {
            // If so, add this server's name into the `relayed_by` array
            Some(_) => {
                match self.custom_fields.get_mut("relayed_by") {
                    Some(Value::Array(ref mut vec)) => vec.push(name),
                    None => {
                        self.custom_fields.insert(
                            "relayed_by".to_string(),
                            Value::Array(vec![name]));
                    },
                    //Malformed message, don't mess with it
                    _ => {}
                }
            },
            // Or just stamp our server's name in the origin
            None => {
                self.custom_fields.insert("origin".to_string(),name);
            }
        }
    }
}
