#![feature(proc_macro_hygiene,decl_macro,never_type)]

#[macro_use] extern crate rocket;
//#[macro_use] extern crate rocket_contrib;

mod config;
mod directory;
mod error;
mod message;
mod rooms;
mod room_id;

use directory::Directory;
use error::{Error,Federated};
use message::Message;
use rooms::{Rooms,RoomState};
use room_id::RoomId;
use rocket::fairing::{Info,Fairing,Kind};
use rocket::http::{Header, ContentType, Method, Status};
use rocket::State;
use rocket::request::{Outcome,FromRequest};
use rocket_contrib::json::Json;
use reqwest::IntoUrl;
use reqwest::blocking::Client;
use std::io::Cursor;
//use rocket_contrib::databases::diesel;

// DB stuff eventually
//#[database("fedichat_db")]
//struct DB(diesel::PgConnection);

#[derive(Clone, Debug)]
struct Fedichat {
    directory: Directory,
    rooms: Rooms,
}
impl Fedichat {
    pub fn new() -> Fedichat {
        Fedichat {
            directory: Directory::new(),
            rooms: Rooms::new(),
        }
    }
}

macro_rules! maybe_forward {
    ($id:ident, $request:ident, $ex:expr) => {{
        continue_forward!($id,$request,"".to_owned(),$ex)
    }};
    // There are two variants of body forwarding
    // One with serialization convenience
    // And one for forwarding raw data
    ($id:ident, $request:ident, $body:expr, $ex:expr) => {{
        //TODO: clone bad
        let body_string = match serde_json::to_string(&$body.clone()){
            Ok(val) => val,
            Err(_) => return Federated::Local(Err(Error::InternalError("Serialization failed")))
        };
        continue_forward!($id,$request,body_string,$ex)
    }};
    ($id:ident, $request:ident, raw; $body:expr, $ex:expr) => {{
        continue_forward!($id,$request,$body,$ex)
    }}
}
macro_rules! continue_forward {
    ($id:ident, $request:ident, $body: expr, $ex:expr) => {{
        // Validate room id. We always need to do this.
        if !$id.is_valid() {
            return Federated::Local(Err(Error::InvalidRoomId));
        }
        if $id.is_local() {
            if let Ok($id) = $id.local().ok_or(Error::InvalidRoomId) {
                Federated::Local($ex)
            } else {
                Federated::Local(Err(Error::InvalidRoomId))
            }
        } else {
            // Forward the request to the responsible server
            let server = match $id.get_address() {
                Some(server) => server,
                None => return Federated::Local(Err(Error::InvalidRoomId))
            };
            Federated::Forwarded({
                let path = format!("http://{}{}",server,$request.get_path());
                println!("Forwarding request to {}",path);
                //http is probably fine here because most servers redirect http to https
                $request.prepare(&path)
                        .body($body)
                        .send()
            })

        }

    }}
}

struct Request<'a> {
    origin: rocket::http::uri::Origin<'a>,
    method: rocket::http::Method
}
impl<'a> Request<'a> {
    //Returns a normalized path as a string for easy forwarding
    pub fn get_path(&self) -> String {
        format!("{}",self.origin.to_normalized())
    }
    //Returns the method to use with reqwest
    //And prepares a builder with it
    pub fn prepare<U: IntoUrl>(&self,url: U) -> reqwest::blocking::RequestBuilder {
        use rocket::http::Method::*;
        let client = Client::new();
        match self.method {
            Get => client.get(url),
            Put => client.put(url),
            Post => client.post(url),
            Delete => client.delete(url),
            Patch => client.patch(url),
            Head => client.head(url),
            Options => unimplemented!(),
            Trace => unimplemented!(),
            Connect => unimplemented!(),
        }

    }
}

impl<'a, 'r> FromRequest<'a, 'r> for Request<'a> {
    type Error = !;

    fn from_request(request: &'a rocket::request::Request<'r>) -> Outcome<Self, Self::Error> {
        Outcome::Success(Request {
            origin: request.uri().clone(),
            method: request.method()

        })
    }
}

#[get("/versions")]
fn versions() -> Result<&'static str, ()> {
    Ok("v1/json")
}

#[get("/features")]
fn features() -> Result<&'static str, ()> {
    Ok("")
}


// Room directory endpoints
// XXX: why is this not federated

#[get("/directory")]
fn directory(state: State<Fedichat>) -> error::Result<String> {
    state.directory.to_json().ok_or(Error::InternalError("Directory serialization failure"))
}

#[get("/directory/<name>")]
fn get_directory_entry(state: State<Fedichat>,name: String) -> error::Result<Json<RoomId>> {
    state.directory.get(&name).ok_or(Error::RoomNameNotFound).map(|x| Json(x))
}

#[put("/directory/<name>",data = "<body>")]
fn put_directory_entry(state: State<Fedichat>,name: String, body: Json<RoomId>) -> error::Result<()> {
    state.directory.insert(name,body.into_inner());
    Ok(())
}

#[delete("/directory/<name>")]
fn delete_directory_entry(state: State<Fedichat>,name: String) -> error::Result<()> {
    state.directory.remove(&name);
    Ok(())
}

// Room endpoints


// Create a new room
// Returns room ID
// The first variant generates a room with a random ID
//
// XXX
// wait a minute
// this can't federate ever
// because we don't have a roomID to check
//#[post("/rooms/", data = "<body>")]
//fn create_random_room(state: State<Fedichat>, req: Request, body: String) -> error::Federated<String> {
//    maybe_forward!(id, req, body, {
//        let room_state = if body == "" { 
//            None 
//        } else { 
//            match serde_json::from_str(&body) {
//                Ok(val) => Some(val),
//                Err(e) => return Federated::Local(Err(Error::InvalidRoomState))
//            }
//        };
//        state.rooms.create_room(id,room_state)
//    })
//}

// The second uses a user-given ID
#[post("/room/<id>", data = "<body>")]
fn create_room(state: State<Fedichat>, req: Request, id: RoomId, body: String) -> error::Federated<String> {
    maybe_forward!(id, req, raw; body, {
        let room_state = if body == "" { 
            None 
        } else { 
            match serde_json::from_str(&body) {
                Ok(val) => Some(val),
                Err(_) => return Federated::Local(Err(Error::InvalidRoomState))
            }
        };
        state.rooms.create_room(id,room_state)
    })
}


// Delete a room
#[delete("/room/<id>")]
fn delete_room(state: State<Fedichat>, req: Request, id: RoomId) -> error::Federated<()> {
    maybe_forward!(id, req, state.rooms.delete_room(id))
}


// Room state endpoints

// Single key
#[get("/room/<room_id>/state/value/<key>")]
fn get_state_value(state: State<Fedichat>, req: Request, room_id: RoomId, key: String) -> error::Federated<String>{
    maybe_forward!(room_id, req, state.rooms.get_state_value(room_id,&key))
}

#[put("/room/<room_id>/state/value/<key>", data = "<body>")]
fn set_state_value(state: State<Fedichat>, req: Request, room_id: RoomId, key: String, body: String) -> error::Federated<String> {
    maybe_forward!(room_id, req, raw; body, state.rooms.set_state_value(room_id,key,body))
}

#[delete("/room/<room_id>/state/value/<key>")]
fn delete_state_value(state: State<Fedichat>, req: Request, room_id: RoomId, key: String) -> error::Federated<()>{
    maybe_forward!(room_id, req, state.rooms.delete_state_value(room_id,&key))
}

// Compare and swap
#[put("/room/<room_id>/state/compare_and_swap/<key>", data = "<body>")]
fn compare_and_swap_value(state: State<Fedichat>, req: Request, room_id: RoomId, key: String, body: Json<[String; 2]>) -> error::Federated<String> {
    maybe_forward!(room_id, req, body, state.rooms.compare_and_swap_value(room_id,key,body.0))
}

// Full state
#[get("/room/<room_id>/state")]
fn get_state(state: State<Fedichat>, req: Request, room_id: RoomId) -> error::Federated<Json<RoomState>>{
    maybe_forward!(room_id, req, state.rooms.get_state(room_id).map(|x| Json(x)))
}

#[put("/room/<room_id>/state", data = "<body>")]
fn put_state(state: State<Fedichat>, req: Request, room_id: RoomId, body: Json<RoomState>) -> error::Federated<()>{
    maybe_forward!(room_id, req, body, state.rooms.put_state(room_id,body.into_inner()))
}

#[patch("/room/<room_id>/state", data = "<body>")]
fn update_state(state: State<Fedichat>, req: Request, room_id: RoomId, body: Json<RoomState>) -> error::Federated<()> {
    maybe_forward!(room_id, req, body, state.rooms.update_state(room_id,body.into_inner()))
}

#[delete("/room/<room_id>/state")]
fn delete_state(state: State<Fedichat>, req: Request, room_id: RoomId) -> error::Federated<()> {
    maybe_forward!(room_id, req, state.rooms.delete_state(room_id))
}



// Room messaging endpoints

#[get("/room/<room_id>/messages?<after>&<before>&<limit>")]
fn get_messages(state: State<Fedichat>, 
                req: Request, 
                room_id: RoomId, 
                after: Option<usize>,
                before: Option<usize>,
                limit: Option<usize>,
                ) -> error::Federated<Json<Vec<Message>>> {
    maybe_forward!(room_id,req,state.rooms.get_messages(room_id,after,before,limit).map(|x| Json(x)))
}

// Returns message ID
#[post("/room/<room_id>/messages", data = "<body>")]
fn send_message(state: State<Fedichat>, req: Request, room_id: RoomId, body: Json<Message>) -> error::Federated<String> {
    maybe_forward!(room_id,req,body,state.rooms.send_message(room_id,body.into_inner()).map(|num| format!("{}",num)))
}

#[get("/room/<room_id>/messages/<message_id>")]
fn get_message(state: State<Fedichat>, req: Request, room_id: RoomId, message_id: u64) -> error::Federated<Json<Message>> {
    maybe_forward!(room_id,req,state.rooms.get_message(room_id,message_id).map(|x| Json(x)))
}

// Handle all CORS requests
pub struct CORS();

impl Fairing for CORS {
    fn info(&self) -> Info {
        Info {
            name: "Add CORS headers to all requests",
            kind: Kind::Response
        }
    }

    fn on_response(&self, request: &rocket::Request, response: &mut rocket::Response) {
        response.set_header(Header::new("Access-Control-Allow-Origin", "*"));
        response.set_header(Header::new("Access-Control-Allow-Methods", "GET,POST,OPTIONS,PUT,DELETE,PATCH"));
        response.set_header(Header::new("Access-Control-Allow-Headers", "Content-Type"));
        response.set_header(Header::new("Access-Control-Allow-Credentials", "true"));

        if request.method() == Method::Options {
            response.set_header(ContentType::Plain);
            response.set_sized_body(Cursor::new(""));
            response.set_status(Status::Ok);
        }
    }
}

pub fn main() {
    let state = Fedichat::new();
    rocket::ignite()
        .mount("/_fedichat/",routes![features,versions])
        .mount("/_fedichat/v1/json",
            routes![
               directory,
               get_directory_entry,
               put_directory_entry,
               delete_directory_entry,
               create_room,
               delete_room,
               get_state_value,
               set_state_value,
               delete_state_value,
               compare_and_swap_value,
               get_state,
               put_state,
               update_state,
               delete_state,
               get_messages,
               send_message,
               get_message
           ])
        .manage(state)
        .attach(CORS())
        .launch();
}
