use rocket::http::{ContentType,Status};
use rocket::{response,Response,Request};
use serde::Serialize;
use std::io::Cursor;
use serde_json;
use rocket::response::Responder;

pub enum Federated<T> {
    Local(self::Result<T>),
    Forwarded(std::result::Result<reqwest::blocking::Response,reqwest::Error>)
}
impl<'r,T: Responder<'r>> Responder<'r> for Federated<T> {
    fn respond_to(self, req: &Request) -> response::Result<'r> {
        match self {
            Federated::Local(res) => res.respond_to(req),
            Federated::Forwarded(Ok(res)) => convert_response(res),
            Federated::Forwarded(Err(err)) => panic!("Bad request: {}",err)
        }
    }
}

fn convert_response(res: reqwest::blocking::Response) -> response::Result<'static> {
    Ok(Response::build()
        .status(Status::from_code(res.status().as_u16()).ok_or(Status::InternalServerError)?)
        .sized_body(Cursor::new(res.bytes().map_err(|_| Status::InternalServerError)?))
        .finalize())

}

pub type Result<T> = std::result::Result<T,Error>;

#[derive(Clone,Debug,PartialEq,Eq)]
pub enum Error {
    KeyNotFound,
    RoomIdNotFound,
    InvalidRoomId,
    RoomNameNotFound,
    MessageNotFound,
    DuplicateRoomId,
    InvalidRoomState,
    InternalError(&'static str)
}

impl Error {
    pub fn error_kind(&self) -> String {
        use Error::*;
        match self {
            InternalError(_) => "InternalError".to_string(),
            e => format!("{:?}",e)
        }
    }
    pub fn message(&self) -> &'static str {
        use Error::*;
        match self {
            KeyNotFound => "The given key was not found in the room's state.",
            RoomIdNotFound => "The room specified does not exist.",
            InvalidRoomId => "The room id is not formatted correctly.",
            RoomNameNotFound => "The room does not appear in the directory.",
            MessageNotFound => "The specified message does not exist.", 
            DuplicateRoomId => "A room with the given id already exists.",
            InvalidRoomState => "Could not parse room state.",
            InternalError(a) => a
        }
    }
    pub fn serialize(&self) -> SerError {
        SerError {
            error_kind: self.error_kind(),
            description: self.message()
        }
    }
    pub fn status_code(&self) -> Status {
        use Error::*;
        match self {
            InternalError(_) => Status::InternalServerError,
            _ => Status::BadRequest,
        }
    }
}

#[derive(Clone,Serialize)]
pub struct SerError {
    pub error_kind: String,
    pub description: &'static str
}

impl<'r> Responder<'r> for Error {
    fn respond_to(self, _: &Request) -> response::Result<'r> {
        let body = serde_json::to_string(&self.serialize()).map_err(|_|Status::InternalServerError);
        Response::build()
            .status(self.status_code())
            .header(ContentType::JSON)
            .sized_body(Cursor::new(body?))
            .ok()
    }
}

