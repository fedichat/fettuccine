// Probably should be in /var/lib eventually or something
pub const DATA_DIRECTORY: &'static str = "./";
pub const SERVER_NAME: &'static str = "fedichat.firechicken.net";

pub fn directory_db_location() -> String {
    DATA_DIRECTORY.to_owned() + "database.db"
}

pub fn room_db_location() -> String {
    DATA_DIRECTORY.to_owned() + "rooms.db"
}
