use crate::config;
use crate::room_id::RoomId;

use serde::Serialize;
use sled;
use std::collections::HashMap;
use std::str;

#[derive(Debug,Clone)]
pub struct Directory {
    directory: sled::Db
}

// how do I use sled
#[derive(Debug,Clone,Serialize)]
pub struct InnerDirectory(HashMap<String,RoomId>);

impl Directory {
    pub fn new() -> Directory {
        let path = config::directory_db_location();
        // TODO: actual error handling
        let db = sled::open(&path)
            .expect(&format!("Could not open database DB at {}",&path));
        Directory {
            directory: db
        }
    }

    /// Convert the room directory into a big json-encoded string
    pub fn to_json(&self) -> Option<String> {
        // Turn sled db into a hashmap
        let map: Option<HashMap<String,String>> = self.directory.iter()
            .map(|tup| match tup {
                Ok((s1,s2)) => Some((str::from_utf8(&s1).ok()?.to_owned(),
                                     str::from_utf8(&s2).ok()?.to_owned())),
                Err(_) => None
            })
            .collect();
        serde_json::to_string(&map).ok()
    }

    /// Get a single directory entry
    /// NOTE: Masks errors. I probably shouldn't do that
    pub fn get(&self, room_name: &str) -> Option<RoomId> {
        self.directory.get(room_name.as_bytes())
            .ok()
            .flatten()
            .map(|s| str::from_utf8(&s).ok().map(|id| RoomId(id.to_owned())))
            .flatten()
    }
    /// Add or update a directory entry
    pub fn insert(&self, room_name: String, room_id: RoomId) {
        let _ = self.directory.insert(room_name.as_bytes(),room_id.0.as_bytes());
    }
    /// Remove a directory entry
    pub fn remove(&self, room_name: &str) {
        let _ = self.directory.remove(room_name.as_bytes());
    }
}
