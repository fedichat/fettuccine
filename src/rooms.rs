use crate::error::{self,Error};
use crate::message::Message;
use crate::room_id::RoomId;
use parking_lot::RwLock;
use std::collections::HashMap;
use std::sync::Arc;

pub type RoomState = HashMap<String,String>;

#[derive(Debug)]
pub struct Room {
    messages: RwLock<Vec<Message>>,
    state: RwLock<RoomState>
}

impl Room {
    pub fn new(state: Option<HashMap<String,String>>) -> Room {
        Room {
            messages: RwLock::new(Vec::new()),
            state: match state {
                        // TODO: stick the room creation timestamp in the state
                        Some (s) => RwLock::new(s),
                        None     => RwLock::new(HashMap::new())
                   }
        }
    }
    pub fn send_message(&self, mut message: Message) -> u64 {
        message.stamp();
        let messages = &mut *self.messages.write();
        let len = messages.len() as u64;
        messages.push(message.add_id(len));
        len
    }
    pub fn get_message(&self, message_id: u64) -> error::Result<Message> {
        (*self.messages.read())
            .get(message_id as usize)
            .cloned()
            .ok_or(Error::MessageNotFound)
    }
    pub fn get_messages(&self, 
                        after: Option<usize>,
                        before: Option<usize>,
                        limit: Option<usize>) -> error::Result<Vec<Message>> {
        let messages = &*self.messages.read();
        let message_list = match (after,before) {
            (Some(after_id),Some(before_id)) => &messages[(after_id+1) .. before_id],
            (Some(after_id),None) => &messages[after_id+1 ..],
            (None,Some(before_id)) => &messages[0 .. before_id],
            (None,None) => &messages[..],
        };
        if let Some(limit) = limit {
            Ok(message_list.iter().take(limit).cloned().collect())
        } else {
            Ok(message_list.iter().cloned().collect())
        }

    }
    pub fn get_state_value(&self,key: &str) -> error::Result<String> {
        (*self.state.read())
            .get(key)
            .cloned()
            .ok_or(Error::KeyNotFound)
    }
    pub fn set_state_value(&self,key: String, value: String) -> error::Result<String> {
        (*self.state.write())
            .insert(key,value.clone());
        // Inserting always succeeds
        Ok(value)

    }
    pub fn delete_state_value(&self,key: &str) -> error::Result<()> {
        (*self.state.write())
            .remove(key)
            // Throw away the return value
            // NOTE: It might be worth returning this to the user?
            // Is it worth it at all?
            .map(|_| ())
            .ok_or(Error::KeyNotFound)
    }
    pub fn compare_and_swap_value(&self, key: String, expected: String, new: String) -> error::Result<String> {
        let state = &mut (*self.state.write());
        match state.get(&key) {
            None => {
                state.insert(key,new.clone());
                Ok(new)
            },
            Some(x) if *x == expected => {
                state.insert(key,new.clone());
                Ok(new)
            },
            Some(x) => Ok(x.clone())
        }
    }
    pub fn get_state(&self) -> error::Result<RoomState> {
        Ok((*self.state.read()).clone())
    }
    pub fn put_state(&self, state: RoomState) -> error::Result<()> {
        (*self.state.write()) = state;
        Ok(())
    }
    pub fn update_state(&self, mut state: RoomState) -> error::Result<()> {
        let old_state = &mut (*self.state.write());
        // Can I get a draining iterator to move out of this?
        for (k,v) in state.drain() {
            old_state.insert(k,v);
        }
        Ok(())
    }
    pub fn delete_state(&self) -> error::Result<()> {
        (*self.state.write()).clear();
        Ok(())
    }
}   

// hmmm how to synchronize these
// because I can actually get race conditions when reading from multiple threads
// HMM CAN I KEEP A MAP HERE
// AND MAKE A SLED DB FOR EACH ROOM?
//
// yeah keep a tree for each room
// that should do it
//
// wait what's the point then?
// god persistence is hard
//
// why don't I just serialize this to disk every so often
// it'd fuck up the rwlock lmao
// not too badly though, you only have to hold the read long enough to make a copy
// and only once a minute or so
//
// tomorrow: is it easier to use postgres?
// it should be pretty stupidly easy to serialize to cbor or bincode or something
// this is dead simple and way faster than using sled
//
// I could totally use sled and it'd probably work fine
// CAS loops aren't too bad
// maybe they are for state variables though
// YOOOOOOO
// THAT'S WHAT I NEED FOR THE STATE SYSTEM
//
// solutions to the sled problem -- swap out data with a None
// like a janky spinlock
// 
// holy shit this is weird
// there's a way to do this completely without locks, with compare_and_swap
// and update_and_fetch wraps that
// this is probably the way to go
// it can call functions multiple times but none of them are that expensive

#[derive(Debug,Clone)]
pub struct Rooms {
    rooms: Arc<RwLock<HashMap<RoomId,Room>>>
}

impl Rooms {
    pub fn new() -> Rooms {
        Rooms {
            rooms: Arc::new(RwLock::new(HashMap::new())),
        }
    }
    pub fn send_message(&self, room_id: RoomId, message: Message) -> error::Result<u64> {
        Ok((*self.rooms.read())
            .get(&room_id)
            .ok_or(Error::RoomIdNotFound)?
            .send_message(message)
        )
    }
    pub fn get_message(&self, room_id: RoomId, message_id: u64) -> error::Result<Message> {
        (*self.rooms.read())
            .get(&room_id)
            .ok_or(Error::RoomIdNotFound)?
            .get_message(message_id)
    }
    pub fn get_messages(&self, 
                        room_id: RoomId, 
                        after: Option<usize>,
                        before: Option<usize>,
                        limit: Option<usize>,
                        ) -> error::Result<Vec<Message>>{
        (*self.rooms.read())
            .get(&room_id)
            .ok_or(Error::RoomIdNotFound)?
            .get_messages(after,before,limit)
    }
    pub fn delete_room(&self, room_id: RoomId) -> error::Result<()> {
        (*self.rooms.write())
            .remove(&room_id)
            .ok_or(Error::RoomIdNotFound)
            .map(|_| ())
    }
    pub fn create_room(&self, room_id: RoomId, state: Option<HashMap<String,String>>) -> error::Result<String> {
        let rooms = &mut (*self.rooms.write());
        if rooms.contains_key(&room_id) {
            return Err(Error::DuplicateRoomId)
        } else {
            rooms.insert(room_id.clone(),Room::new(state));
            Ok(room_id.0)
        }

    }
    pub fn get_state_value(&self, room_id: RoomId, key: &str) -> error::Result<String> {
        (*self.rooms.read())
            .get(&room_id)
            .ok_or(Error::RoomIdNotFound)?
            .get_state_value(key)
    }
    pub fn compare_and_swap_value(&self, room_id: RoomId, key: String, [expected,new]: [String; 2]) -> error::Result<String> {
        (*self.rooms.write())
            .get(&room_id)
            .ok_or(Error::RoomIdNotFound)?
            .compare_and_swap_value(key,expected,new)
    }
    pub fn set_state_value(&self, room_id: RoomId, key: String, value: String) -> error::Result<String> {
        (*self.rooms.read())
            .get(&room_id)
            .ok_or(Error::RoomIdNotFound)?
            .set_state_value(key,value)
    }
    pub fn delete_state_value(&self, room_id: RoomId, key: &str) -> error::Result<()> {
        (*self.rooms.read())
            .get(&room_id)
            .ok_or(Error::RoomIdNotFound)?
            .delete_state_value(key)
    }
    pub fn get_state(&self, room_id: RoomId) -> error::Result<RoomState> {
        (*self.rooms.read())
            .get(&room_id)
            .ok_or(Error::RoomIdNotFound)?
            .get_state()
    }
    pub fn put_state(&self, room_id: RoomId, state: RoomState) -> error::Result<()> {
        (*self.rooms.read())
            .get(&room_id)
            .ok_or(Error::RoomIdNotFound)?
            .put_state(state)
    }
    pub fn update_state(&self, room_id: RoomId, state: RoomState) -> error::Result<()> {
        (*self.rooms.read())
            .get(&room_id)
            .ok_or(Error::RoomIdNotFound)?
            .update_state(state)
    }
    pub fn delete_state(&self, room_id: RoomId) -> error::Result<()> {
        (*self.rooms.read())
            .get(&room_id)
            .ok_or(Error::RoomIdNotFound)?
            .delete_state()
    }
}

