use rocket::request::FromParam;
use rocket::http::RawStr;
use serde::{Serialize,Deserialize};
use std::str::Utf8Error;
use crate::config::SERVER_NAME;

// Hmm this one is kind of tricky
// this should be a string of the form <num>,<num>,...,<num>
// but it's probably easiest to work with if it's a plain string
#[derive(Debug,Clone,Hash,PartialEq,Eq,Serialize,Deserialize)]
pub struct RoomId(pub String);

//TODO: Do some real parsing
//      There's probably a ton of awful edge cases here

impl RoomId {
    pub fn is_local(&self) -> bool{
        !self.0.contains("@")
        || match self.get_address() {
            Some(a) => a == SERVER_NAME,
            None => false
           }
    }
    pub fn get_address(&self) -> Option<&str> {
        self.0.split('@').skip(1).next()
    }
    /// This converts a room id to its local component
    pub fn local(&self) -> Option<RoomId> {
        Some(RoomId(self.0.split('@').next()?.to_owned()))
    }
    pub fn split(&self) -> Option<(&str,&str)> {
        let mut iter = self.0.split('@');
        let a = iter.next();
        let b = iter.next();
        Some((a?,b?))
    }
    pub fn is_valid(&self) -> bool {
        // Get the local part of the id
        // or return that the id is invalid
        // because it is empty
        let id = match self.local() {
            Some(s) => s,
            None => return false
        };
        for ch in id.0.chars() {
            match ch {
                '0'|'1'|'2'|'3'|'4'|'5'|'6'|'7'|'8'|'9'|',' => continue,
                _ => return false
            };
        }
        return true
    }
}

#[cfg(test)]
mod test {
    use super::RoomId;
    #[test]
    fn local_addr_test() {
        assert!(RoomId("1,2,3".to_string()).is_local());
        assert!(RoomId("1,2,3@fedichat.firechicken.net".to_string()).is_local());
        assert!(!RoomId("1,2,3@address.com".to_string()).is_local());
    }
    #[test]
    fn local_conversion_test() {
        assert_eq!(RoomId("1,2,3@fedichat.firechicken.net".to_string()).local(),
                   Some(RoomId("1,2,3".to_string())));
    }
    #[test]
    fn extract_address() {
        assert_eq!(None,RoomId("1,2,3".to_string()).get_address());
        assert_eq!(Some("foo.com"),RoomId("1,2,3@foo.com".to_string()).get_address());
    }
    #[test]
    fn validate_id() {
        assert!(RoomId("0,1,2".to_string()).is_valid());
        assert!(RoomId("0,1,2@foo.bar".to_string()).is_valid());
        assert!(!RoomId("lmao".to_string()).is_valid());
    }
}

impl<'r> FromParam<'r> for RoomId {
    type Error = Utf8Error;

    fn from_param(param: &'r RawStr) -> Result<Self, Self::Error> {
        param.url_decode().map(|s| RoomId(s))
    }
}

