# fetuccine
### A fedichat relay server

Fettuccine is a prototype fedichat server written in Rust. Fettuccine is built
on top of Rocket, a Rust web framework, and it should be relatively fast. 

## Setup
Fettuccine needs no database currently, as everything is volatile and stored in memory,
so configuration is incredibly simple. The only thing you'll have to do is edit
src/config.rs with a different `SERVER_NAME`. It's encouraged to run fettuccine
behind a nginx reverse proxy but you can probably get away without it. You can
change the `ROCKET_PORT` environment variable to change what port fettuccine
listens on, if you want.

## Spec
Fedichat spec:
https://hackmd.io/jt1xuIPeTJ-lUu0VIoz99w
